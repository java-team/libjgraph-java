libjgraph-java (5.12.4.2+dfsg-7) unstable; urgency=medium

  * Team upload
  * Adding a fake site to explain about the current status of the original
    upstream homepage

 -- Pierre Gruet <pgt@debian.org>  Sun, 13 Nov 2022 23:19:37 +0100

libjgraph-java (5.12.4.2+dfsg-6) unstable; urgency=medium

  * Team upload
  * Raising Standards version to 4.6.1:
    - Using https form of copyright-format URI
    - Priority extra is now optional
    - Rules-Requires-Root: no
    - Deleting unnecessary get-orig-source target in d/rules
    - Repacking through Files-Excluded in d/copyright
  * Renaming BSD-3 stanza to BSD-3-clause in d/copyright
  * Raising d/watch version to 4, rewriting it a bit
  * Depending on debhelper-compat 13, dropping d/compat
  * Vcs-* fields now point to Salsa
  * Set upstream metadata fields: Repository-Browse.
  * Pointing URI to the Github repo in which the code now lies, adding
    a d/README.source file
  * Marking libjgraph-java and libjgraph-java-doc as Multi-Arch: foreign
  * Correcting paths in the doc-base file
  * Removing duplicate documentation files
  * Adding a Lintian override for the embedded Javascript

  [ gregor herrmann ]
  * Remove myself from Uploaders.

 -- Pierre Gruet <pgt@debian.org>  Mon, 22 Aug 2022 23:04:17 +0200

libjgraph-java (5.12.4.2+dfsg-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 01:37:55 +0100

libjgraph-java (5.12.4.2+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Add maven artifacts, build-depend upon maven-repo-helper.

 -- tony mancill <tmancill@debian.org>  Sun, 27 Nov 2016 22:19:07 -0800

libjgraph-java (5.12.4.2+dfsg-4) unstable; urgency=medium

  * Team upload.
  * Fix Git repository URL and use libjgraph-java instead of libspin-java.

 -- Markus Koschany <apo@debian.org>  Fri, 21 Oct 2016 22:33:27 +0200

libjgraph-java (5.12.4.2+dfsg-3) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Update debian/repack.stub.

  [ Markus Koschany ]
  * Switch to compat level 10.
  * wrap-and-sort -sa.
  * Use only Build-Depends field.
  * Vcs fields: Use cgit and https.
  * Update Homepage field and debian/copyright Source field.
    JGraph 5 has been discontinued and is called legacy-jgraph5 on Github now.
    The new project name is JGraphX. (Closes: #822457)
  * Declare compliance with Debian Policy 3.9.8.
  * Switch to copyright format 1.0, fix syntax errors and remove unused
    paragraphs.

 -- Markus Koschany <apo@debian.org>  Fri, 21 Oct 2016 22:01:51 +0200

libjgraph-java (5.12.4.2+dfsg-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/watch: fix dversionmangle.

  [ tony mancill ]
  * Update Maintainer field for Java team maintenance.
  * Add gregor to Uploaders.
  * Update Vcs-* headers.
  * Set Standards-Version to 3.9.3 (no changes).
  * Remove needless dependency on JRE.

 -- tony mancill <tmancill@debian.org>  Mon, 28 May 2012 22:32:30 -0700

libjgraph-java (5.12.4.2+dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - set Section to java
    - change build dependency from default-jdk-builddep to default-jdk
    - add ${misc:Depends} to Depends:.
  * Change source URL in debian/copyright and debian/watch.
  * Switch from dh_wraporig to repack.sh for repackaging.
  * Switch to source format 3.0 (quilt).
  * Set Standards-Version to 3.9.1 (no changes).
  * Use debhelper 7 (debian/{rules,compat,control})
  * Update patch and patch headers.
  * debian/rules: don't build apidocs twice.
  * debian/copyright: update years of copyright and formatting.
  * Register API specification with doc-base system.

 -- gregor herrmann <gregoa@debian.org>  Wed, 09 Feb 2011 18:37:22 +0100

libjgraph-java (5.12.2.1.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Set Standards-Version to 3.8.0; rename debian/README.Debian-source to
    debian/README.source and add pointer to quilt howto.
  * Refresh patch 01_buildxml.patch.
  * Change runtime dependency to "default-jre-headless |
    java-runtime-headless".
  * Move examples from the library packages to the doc package.

 -- gregor herrmann <gregoa@debian.org>  Sat, 15 Nov 2008 15:09:26 +0100

libjgraph-java (5.12.1.0.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Switch from dpatch to quilt.
  * Change my email address in several files under debian/.
  * debian/copyright: update and compact.
  * debian/control: build depend on default-jdk-builddep, depend on
    default-jre instead of specifying a specific JDK/JRE; change JAVA_HOME
    in debian/rules accordingly.
  * Install API documentation to /usr/share/doc/libjgraph-java-doc/api/
    instead of directly under libjgraph-java-doc/

 -- gregor herrmann <gregoa@debian.org>  Fri, 09 May 2008 18:29:56 +0200

libjgraph-java (5.12.0.4.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Remove debian/dirs and create /usr/share/java directly from
    debian/rules.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 17 Mar 2008 20:28:08 +0100

libjgraph-java (5.12.0.1.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Add build.xml to files with copyright "-2008" in debian/copyright.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 24 Feb 2008 20:40:50 +0100

libjgraph-java (5.12.0.0.dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright:
    - add stanza for file with copyright -2008
    - replace LGPL2+ with LGPL-2+
    - add missing space
  * debian/rules:
    - remove some spurious whitespace
    - remove configure{,-stamp} targets

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Tue, 19 Feb 2008 18:38:24 +0100

libjgraph-java (5.11.0.1.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Thu, 31 Jan 2008 17:39:58 +0100

libjgraph-java (5.10.2.0.dfsg-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/rules: explicitly set JAVA_HOME.
  * Change debian/copyright to the new machine-readable format.
  * Set debhelper compatibility level to 6.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Thu, 10 Jan 2008 19:57:39 +0100

libjgraph-java (5.10.2.0.dfsg-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream release.
  * Move upstream URL from the description to the new Homepage field.
  * Change XS-Vcs-* fields to Vcs-*.
  * debian/rules: let all targets depend on *-stamp targets.

  [ tony mancill ]
  * debian/control: remove trailing spaces from Homepage field.
  * Set Standards Version to 3.7.3.

 -- tony mancill <tmancill@debian.org>  Thu, 13 Dec 2007 20:01:54 -0800

libjgraph-java (5.10.1.5.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Clean up debian/rules.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 17 Sep 2007 18:45:29 +0200

libjgraph-java (5.10.1.4.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 03 Aug 2007 15:08:06 +0200

libjgraph-java (5.10.1.3.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Improve long and short descriptions of binary packages.
  * Update years of copyright in debian/copyright.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Fri, 27 Jul 2007 13:03:58 +0200

libjgraph-java (5.10.1.2.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Patch build.xml to stop it from fetching external stuff over the net
    when building the docs.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 09 Jun 2007 17:17:02 +0200

libjgraph-java (5.10.1.0.dfsg-1) unstable; urgency=low

  * New upstream release.
  * Improve debian/watch and debian/rules.
  * Use dh_wraporig to create .dfsg tarball:
    - Add debian/dh_wraporig.local.
    - Include created debian/README.Debian-source.
    - Adapt debian/watch to call dh_wraporig.
    - Change get-orig-source target in debian/rules to use uscan/dh_wraporig.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 13 May 2007 02:12:09 +0200

libjgraph-java (5.10.0.1.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon, 26 Feb 2007 17:25:47 +0100

libjgraph-java (5.9.2.1.dfsg-1) unstable; urgency=low

  * Initial release (closes: #404515).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun, 14 Jan 2007 21:35:23 +0100
