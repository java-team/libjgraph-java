Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Gaudenz Adler
Upstream-Name: JGraph
Source: https://github.com/jgraph/legacy-jgraph5
Files-Excluded: examples/org/jgraph/example/JGraphAbstractIconAnimator.java
Comment:
 The file ./examples/org/jgraph/example/JGraphAbstractIconAnimator.java
 was removed from the upstream tarball because it has a non-free license.

Files: *
Copyright: 2001-2008, Gaudenz Alder
License: LGPL-2+

Files: examples/com/jgraph/example/portlabels/PortLabelCell.java
       examples/com/jgraph/example/portlabels/PortLabelVertexView.java
       examples/com/jgraph/example/fastgraph/FastGraphFactory.java
Copyright: 2005-2006, David Benson
License: LGPL-2+

Files: examples/com/jgraph/example/portlabels/PortLabelGraph.java
Copyright: 2005, Gaudenz Alder
           2005-2006, David Benson
License: LGPL-2+

Files: src/org/jgraph/util/Spline2D.java
Copyright: 2003, Martin Krueger
           2005, David Benson
License: LGPL-2+

Files: src/org/jgraph/util/Spline.java
Copyright: "@author krueger"  (ostensibly Martin Kruger)
License: public-domain
 This file was granted to the Public Domain.

Files: examples/org/jgraph/example/JGraphIconView.java
       examples/org/jgraph/example/IconExample.java
Copyright: 2001-2004, Dean Mao All rights reserved.
License: BSD-3-clause

Files: examples/com/jgraph/example/panelexample/PortLabelGraph.java
Copyright: 2005, Gaudenz Alder
           2005, David Benson
License: LGPL-2+

Files: examples/com/jgraph/example/GraphEdX.java
       examples/com/jgraph/example/JGraphGraphFactory.java
Copyright: 2001-2006, Gaudenz Alder
           2005-2006, David Benson
License: LGPL-2+

Files: src/org/jgraph/util/Bezier.java
       examples/com/jgraph/example/panelexample/PortLabelCell.java
       examples/com/jgraph/example/fastgraph/FastCircleCell.java
       examples/com/jgraph/example/fastgraph/FastCellViewFactory.java
       examples/com/jgraph/example/fastgraph/FastEdge.java
       examples/com/jgraph/example/fastgraph/FastVertexView.java
       examples/com/jgraph/example/fastgraph/FastEdgeView.java
       examples/com/jgraph/example/fastgraph/FastGraphUI.java
       examples/com/jgraph/example/fastgraph/FastPortView.java
Copyright: 2005, David Benson
License: LGPL-2+

Files: examples/com/jgraph/example/fastgraph/FastGraph.java
       examples/com/jgraph/example/fastgraph/FastGraphModel.java
Copyright: 2001-2005, Gaudenz Alder
           2005, David Benson
License: LGPL-2+

Files: debian/*
Copyright: 2006-2011, gregor herrmann <gregoa@debian.org>
 2006-2011, tony mancill <tmancill@debian.org>
License: LGPL-2+

License: LGPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer. - Redistributions in
 binary form must reproduce the above copyright notice, this list of
 conditions and the following disclaimer in the documentation and/or other
 materials provided with the distribution. - Neither the name of JGraph nor
 the names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
